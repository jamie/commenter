# Commenter

Commenter is a very simple javascript/AJAX based blog commenting system for
statically generated sites that fools bots via a hidden email field and
(optionally) hcaptcha. Comments are stored in a sqlite3 database.

## Install

Just copy `config.php.sample` to `config.php`, `index.html.sample` to
`index.html` and upload this directory to your web root. To keep things clean
you may want to put it in a directory called `/commenter`.

Then, access `index.html` via your web browser to see this script in action.

If you want to implement it on your web site, examine `index.html.sample` to
learn what you need to add to your site.

### Extra

If you want to support markdown syntax in comments, then run:

    composer install

To pull in the [parsedown](https://parsedown.org/) library. This is optional.

## Usage

When a comment is posted to your site, you will receive an email notifying you
so you can approve or reject it before it is publicall visible.

Whever you receive an email, run `php comment.php review` via the command line
to review and approve or reject comments.

## Maintenance

A log is kept of every post, including ones rejected due to a bot entering a
value for the hidden email field or a hcaptcha failure.

To review the log, run `php comment.php log`.
