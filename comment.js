function commenter() {

  function displayComments(response) {
    if (response['is_error'] == 1) {
      tellUser(response['error']);
    }
    else {
      var parentDiv = document.getElementById('commenter-comments');
      for (var i = 0; i < response.values.length; i++) {
        var comment = response.values[i];
        var divRow = document.createElement('div');

        var divMeta = document.createElement('div');
        var author = document.createElement('span');
        author.className = 'commenter-author';
        author.innerHTML = comment['author'];

        var date = document.createElement('span');
        var dateObj = new Date(comment['timestamp'] * 1000);
        date.className = 'commenter-timestamp';
        date.innerHTML = dateObj.toUTCString();

        var divComment = document.createElement('div');
        divComment.className = 'commenter-comment';
        divComment.innerHTML = comment['comment'];

        divMeta.appendChild(author);
        divMeta.appendChild(date);
        divRow.appendChild(divMeta);
        divRow.appendChild(divComment);
        parentDiv.appendChild(divRow);
      }
    }
  }

  function tellUser(msg) {
    alert(msg);
  }

  function buildForm() {
    var parentDiv = document.getElementById('commenter-form');

    var form = document.createElement('form');
    form.id = 'commenter-form';

    // The email field is a trick, hidden by css
    // and if filled out we reject the comment as spam.
    var emailDiv = document.createElement('div');
    emailDiv.id = 'commenter-email-div';
    var email = document.createElement('input');
    email.id = 'commenter-email';
    email.placeholder = 'Your Email';
    email.style.display = 'none';
    emailDiv.appendChild(email);

    var authorDiv = document.createElement('div');
    authorDiv.id = 'commenter-author-div';
    var author = document.createElement('input');
    author.id = 'commenter-author';
    author.placeholder = 'Your name';
    authorDiv.appendChild(author);

    var commentDiv = document.createElement('div');
    commentDiv.id = 'commenter-comment-div';
    var comment = document.createElement('textarea');
    comment.id = 'commenter-comment';
    comment.placeholder = 'Your comment';
    commentDiv.appendChild(comment);

    if (hcaptchaSiteKey) {
      var hcaptcha = document.createElement('div');
      hcaptcha.className = "h-captcha";
      hcaptcha.dataset.sitekey = hcaptchaSiteKey;
    }

    var buttonDiv = document.createElement('div');
    buttonDiv.id = 'commenter-button-div';
    var button = document.createElement('button'); 
    button.id = 'commenter-submit';
    button.innerHTML = 'Submit';
    button.addEventListener('click', function(event) {
      event.preventDefault();
      var data = {
        comment: comment.value,
        author: author.value,
        path: path,
        email: email.value
      };
      if (hcaptchaSiteKey) {
        captcha_input = document.getElementsByName('h-captcha-response')[0].value;
        if (captcha_input == "") {
          tellUser("Please fill out the captcha.");
          return;
        }
        data['captcha'] = captcha_input;
      }
      
      console.log("Sending", data);
      send('POST', data, thanksForComment, console.log);

    });
    buttonDiv.appendChild(button);

    form.appendChild(emailDiv);
    form.appendChild(authorDiv);
    form.appendChild(commentDiv);

    if (hcaptchaSiteKey) {
      form.appendChild(hcaptcha);
    }
    form.appendChild(buttonDiv);
    parentDiv.appendChild(form);

  }

  function thanksForComment(response) {
    if (response['is_error'] == 1) {
      tellUser(response['error']);
    }
    else {
      var formDiv = document.getElementById('commenter-form');
      formDiv.innerHTML = '<span class="commenter-thanks">Thank you for your post. It will be reviewed by a moderator before being made public.</span>';
    }
  }

  function send(method, params, onSuccess, onError) {
    var request = new XMLHttpRequest();
    var url = commenterUrl;
    var dataElements = [];
    for (var property in params) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(params[property]);
      dataElements.push(encodedKey + "=" + encodedValue);
    }
    var data = dataElements.join("&");
    if (method == 'GET') {
      url += '?' + data;
      data = null;
    }
    request.open(method, url);
    if (method == 'POST') {
      request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    }
    request.onreadystatechange = function() {
      if (request.readyState === 4) {
        if (request.status >= 200 && request.status < 400) {
          if (request.responseText) {
            try {
              onSuccess(JSON.parse(request.responseText));
            }
            catch (err) {
              console.log(err);
              onError(err);
            }
          }
        } else {
          onError(new Error('Response returned with non-OK status'));
          console.log(data);
          console.log(request);
        }
      }
    };
    request.send(data);
  }
  var url = new URL(window.location.href);
  var path = url.pathname;
  data = {
    path: path
  }
  send('GET', data, displayComments, tellUser); 
  buildForm(); 
}

commenter();
