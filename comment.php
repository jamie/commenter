<?php

if (file_exists('vendor/autoload.php')) {
  require('vendor/autoload.php');
}

$c = new commenter('config.php');

if(php_sapi_name()==="cli") {
  $command = $_SERVER['argv'][1];
  if ($command == 'initialize') {
    $c->initialize_db();
  }
  else if ($command == 'review') {
    $c->review();
  }
  else if ($command == 'log') {
    $c->display_log();
  }
  else {
    echo "Unknown subcommend: $command\n";
    exit(1);
  }
}
else {
  $c->run();
}

class commenter {

  protected $hcaptcha_key;
  protected $notify_email;
  protected $db;

  function __construct($config_path) {
    require_once($config_path);
    if (array_key_exists('hcaptcha_key', $config)) {
      $this->hcaptcha_key = $config['hcaptcha_key'];
    }
    if (array_key_exists('notify_email', $config)) {
      $this->notify_email = $config['notify_email'];
    }
    $this->db = new SQLite3($config['database_path']);
    $this->db->busyTimeout(5000);
    $this->db->exec('PRAGMA journal_mode = wal;');
  }
  
  function review() {
    $sql = "SELECT id, path, author, comment FROM comment WHERE status = 0";
    $st = $this->db->prepare($sql);
    $result = $st->execute();
    while($row = $result->fetchArray(SQLITE3_ASSOC)) {
      $a = $this->get_answer($row);    
      if ($a == 'i') {
        echo "\n  The comment has been ignored.\n\n";
        continue;
      }
      else if ($a == 'a') {
        $sql = "UPDATE comment SET status = 1 WHERE id = :id";
        $st = $this->db->prepare($sql);
        $st->bindValue(':id', $row['id']);
        $st->execute();
        echo "\n  The comment has been approved.\n\n";
      }
      else if ($a == 'd') {
        $sql = "DELETE FROM comment WHERE id = :id";
        $st = $this->db->prepare($sql);
        $st->bindValue(':id', $row['id']);
        $st->execute();
        echo "\n  The comment was deleted.\n\n";
      }
    }
  }

  function display_log() {
    $lines = file('log.txt');
    foreach($lines as $line) {
      $line = trim($line);
      if (empty($line)) {
        continue;
      }
      $parts = explode("\t", $line);
      $date = $parts[0];
      $message = $parts[1];
      $details = $parts[2];
      echo "${date}: ${message}\n";
      print_r(json_decode($details));
    }
  } 

  function get_answer($row) {
    $id = $row['id'];
    $path = $row['path'];
    $author = $row['author'];
    $comment = $row['comment'];
    echo "$id | $path | $author\n";
    echo "---\n";
    echo "$comment\n";
    echo "---\n";
    echo "Ignore, Delete or Approve? [Ida] ";
    $allowed = [ 'i', 'd', 'a' ];
    $handle = fopen ("php://stdin","r");
    $a = strtolower(trim(fgets($handle)));
    if (empty($a)) {
      // Ignore by default.
      $a = 'i';
    }
    if (in_array($a, $allowed)) {
      return $a;
    } 
    else {
      echo "Please entire i, d or a.\n";
      return $this->get_answer($row);
    }
  }

  function log($msg) {
    $date = date('Y-m-d H:m:i');
    $log = [];
    $log['_SERVER'] = $_SERVER;
    $log['_REQUEST'] = $_REQUEST;
    $out = "${date}\t${msg}\t" . str_replace("\n", " ", json_encode($log));
    file_put_contents('log.txt', $out,  FILE_APPEND);
  }

  function initialize_db() {
    $sql = "CREATE TABLE IF NOT EXISTS comment 
      (id INTEGER PRIMARY KEY, status INT, path TEXT, timestamp INT, author TEXT, comment TEXT)";
    $this->db->exec($sql);
    $sql = "CREATE index indx_path on comment (path)";
    $this->db->exec($sql);
    $sql = "CREATE index indx_status on comment (status)";
    $this->db->exec($sql);
  }

  function output_json($data) {
    // Fill in extra values
    if (array_key_exists('error', $data)) {
      $data['is_error'] = 1;
    }
    else {
      $data['is_error'] = 0;
    }
    echo json_encode($data);
  }

  function verify_hcaptcha($response) {
    $data = [
      'secret' => $this->hcaptcha_key,
      'response' => $response
    ];
    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "https://hcaptcha.com/siteverify");
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($verify);
    $responseData = json_decode($response);
    if($responseData->success) {
      return TRUE;
    } 
    return FALSE; 
  } 

  function insert_comment($path, $author, $comment) {
    $sql = "INSERT INTO comment VALUES(NULL, 0, :path, :timestamp, :author, :comment )";
    $st = $this->db->prepare($sql);
    $st->bindValue(':path', $path);
    $st->bindValue(':timestamp', time());
    $st->bindValue(':author', $author);
    $st->bindValue(':comment', $comment);
    $st->execute();
    $this->notify($comment, $path);
    $this->output_json(['success' => 'Comment was posted']);
  }

  function notify($comment, $path) {
    if ($this->notify_email) {
      $subject = "You have a new comment";
      $message = "Someone posted a comment on the page:\n\n" . 
        'https://' . $_SERVER['SERVER_NAME'] .  $path . "\n\n" .
        "The comment is: \n\n" .
        htmlentities($comment) . "\n\n" .
        "It needs to be approved or deleted.\n";
      mail($this->notify_email, $subject, $message);
    }
  }

  function validate_post() {
    $required = ['path', 'comment', 'author' ];
    if (!empty($this->hcaptcha_key)) {
      $required['captcha' ];
    }
    foreach($required as $key) {
      if (empty($_POST[$key])) {
        $this->output_json(['error' => "Please enter something in the $key field."]);
        return FALSE;
      }
    }

    // The email is a trick field that should be hidden.
    if (!empty(($_POST['email']))) {
      $this->log("entered email");
      $this->output_json(['error' => "The email field is for tricking bots. If you are a human, please leave it empty."]);
      return FALSE;
    }

    // No html tags in the author fields
    if ($_POST['author'] != strip_tags($_POST['author'])) {
      $this->output_json(['error' => "Please don't include HTML in the author field."]);
      return FALSE;
    }
    if (strlen($_POST['author']) > 64) {
      $this->output_json(['error' => "Please limit the author field to 64 or less characters."]);
      return FALSE;
    }
    if (strlen($_POST['comment']) < 10) {
      $this->output_json(['error' => "Please enter more then 10 characters in the comment field."]);
      return FALSE;
    }
    return TRUE;
  }
  function run() {
    if (isset($_POST['path'])) {
      if (!$this->validate_post()) {
        return;
      }
      $path = $_POST['path'];
      $comment = $_POST['comment'];
      $author = $_POST['author'];
      $hcaptcha = $_POST['captcha'];
      // Require captcha - but only if present.
      if (empty($this->hcaptcha_key) || $this->verify_hcaptcha($hcaptcha)) {
        $this->log("successfully posted comment");
        $this->insert_comment($path, $author, $comment);
      }
      else {
        $this->log("failed to validate captcha");
        $this->output_json(['error' => 'failed to validate captcha']);
      }
    }
    elseif (isset($_GET['path'])) {
      $path = $_GET['path'];
      // Provide list of approved comments.
      $sql = "SELECT timestamp, author, comment FROM comment WHERE path = :path AND status = 1
        ORDER BY timestamp ASC";
      $st = $this->db->prepare($sql);
      $st->bindValue(':path', $path);
      $result = $st->execute();
      $ret = [];
      $parsedown = NULL;
      if (class_exists('Parsedown')) {
        $parsedown = new Parsedown();
        $parsedown->setSafeMode(TRUE);
      }
      while($row = $result->fetchArray(SQLITE3_ASSOC)) {
        if ($parsedown) {
          $comment = $parsedown->text($row['comment']);
        }
        else {
          $comment = nl2br(htmlentities($row['comment']));
        }
        $row['comment'] = $comment;
        $row['author'] = htmlentities($row['author']);
        $ret[] = $row; 
      }
      $this->output_json(['values' => $ret]);
    }
  } 
}

?>
